
FROM rightmesh/ubuntu-openjdk:18.04

USER root

RUN apt-get update && \
  apt-get install build-essential -y && \
  apt-get install gcc-msp430 msp430-libc -y  && \
  apt-get install gcc-avr avr-libc ant -y && \  
  apt-get install libncurses5-dev -y 

WORKDIR /home/$(USER)
RUN git clone https://github.com/contiki-os/contiki.git contiki-3.0 && cd contiki-3.0/ && git submodule update --init