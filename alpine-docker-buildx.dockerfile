FROM alpine:latest AS fetcher

ENV BUILDX_VERSION=0.5.1

RUN apk update && apk add curl

RUN curl -L --output /docker-buildx \
  "https://github.com/docker/buildx/releases/download/v${BUILDX_VERSION}/buildx-v${BUILDX_VERSION}.linux-amd64" \
  && chmod a+x /docker-buildx

FROM docker:latest

RUN apk update && apk add bash

COPY --from=fetcher /docker-buildx /usr/lib/docker/cli-plugins/docker-buildx