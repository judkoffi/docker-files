FROM docker:20.10.2

RUN apk update
RUN apk add maven
RUN apk add zip
RUN apk add openjdk11
RUN apk add npm 
RUN apk add git
RUN apk add curl
RUN apk add wget
RUN apk add openssh
RUN apk add jq
RUN apk add bash
RUN apk add postgresql-client
RUN npm install -g @angular/cli
RUN npm install -g @openapitools/openapi-generator-cli

ENV JAVA_HOME /usr/lib/jvm/java-11-openjdk

RUN apk add dbus-x11 firefox chromium xvfb 

ENV CHROME_BIN /usr/bin/chromium-browser