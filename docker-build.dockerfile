FROM docker:20.10.16

RUN apk update
RUN apk add openjdk17
RUN apk add npm
RUN apk add yarn
RUN apk add bash

ENV JAVA_HOME /usr/lib/jvm/java-17-openjdk